# TypeScript のプロジェクトに ESLint と Prettier を導入する
- ESLint: 構文チェックツール
- Prettier: コードフォーマッター

VSCode の拡張機能も利用する


## 実行環境
- Windows 10 Home (64bit 1909)
    - nvm (1.1.7)
        - Node.js (v10.19.0)
        - npm (6.13.4)
    - VSCode (1.52.1)
        - [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
        - [esbenp.prettier-vscode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)


## 公式ドキュメント
- [Configuring ESLint - ESLint - Pluggable JavaScript linter](https://eslint.org/docs/user-guide/configuring)
- [@typescript-eslint/parser  -  npm](https://www.npmjs.com/package/@typescript-eslint/parser)
- [What is Prettier? · Prettier](https://prettier.io/docs/en/index.html)


## 導入手順
1. npm モジュールのインストール  
    `npm install --save-dev typescript eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin prettier eslint-config-prettier`
1. [`tsconfig.json`](./tsconfig.json) の作成
1. [`tsconfig.eslint.json`](./tsconfig.eslint.json) の作成
1. [`.eslintrc.json`](./eslintrc.json) の作成
1. [`.eslintignore`](./eslintignore) の作成
1. [`.prettierrc.json`](.prettierrc.json) の作成
1. [`.prettierignore`](./prettierignore) の作成
1. [`package.json`](./package.json) に npm スクリプトの記述
    ```json
    "scripts": {
      "lint": "eslint src/**/*.ts",
      "format": "prettier --write src/**/*.ts"
    }
    ```
1. VSCode のワークスペース設定ファイル (`.code-workspace`) に以下の設定を記述
    ```jsonc
    "settings": {
      // 使用するフォーマッタを prettier に
      "editor.defaultFormatter": "esbenp.prettier-vscode",
      // ファイル保存時に自動フォーマットを有効
      "editor.formatOnSave": true
    }
    ```


## ESLint のみを導入する手順
`prettier`、`eslint-config-prettier` はインストールしない。  
`.eslintrc.json` と `package.json` の内容が少し変わるのみでほぼ同じ手順で導入できる。  
VSCode ワークスペースのフォーマット設定も不要。
1. npm モジュールのインストール  
    `npm install --save-dev typescript eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin`
1. [`tsconfig.json`](./tsconfig.json) の作成
1. [`tsconfig.eslint.json`](./tsconfig.eslint.json) の作成
1. [`.eslintrc.json`](./onlyESLint/.eslintrc.json) の作成
1. [`.eslintignore`](./eslintignore) の作成
1. prettier による `format` はできないので `package.json` には `lint` スクリプトのみを記述
    ```json
    "scripts": {
      "lint": "eslint src/**/*.ts",
    }
    ```


## 参考
- [TypeScript + Node.jsプロジェクトにESLint + Prettierを導入する手順2020 - Qiita](https://qiita.com/notakaos/items/85fd2f5c549f247585b1)
- [.prettierrc](https://qiita.com/takeshisakuma/items/bbb2cd2f1c65de70e363)
